package config;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public abstract class BaseTests {
    // Configuration de test
    // Lest tests doivent utiliser le profile "test", donc se connecter à H2 DB
}