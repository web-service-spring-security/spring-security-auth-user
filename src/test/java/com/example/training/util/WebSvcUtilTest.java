package com.example.training.util;

import config.BaseTests;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class WebSvcUtilTest extends BaseTests {

    @Test
    public void testIsNullOrEmpty_withNull() {
        assertTrue(WebSvcUtil.isNullOrEmpty(null), "Expected true for null input");
    }

    @Test
    public void testIsNullOrEmpty_withEmptyString() {
        assertTrue(WebSvcUtil.isNullOrEmpty(""), "Expected true for empty string");
    }

    @Test
    public void testIsNullOrEmpty_withNonEmptyString() {
        assertFalse(WebSvcUtil.isNullOrEmpty("test"), "Expected false for non-empty string");
    }

    @Test
    public void testIsNullOrEmpty_withWhitespaceString() {
        assertFalse(WebSvcUtil.isNullOrEmpty(" "), "Expected false for whitespace string");
    }

    @Test
    public void testIsNullOrEmpty_withSpecialCharacters() {
        assertFalse(WebSvcUtil.isNullOrEmpty("@#&"), "Expected false for special characters");
    }
}