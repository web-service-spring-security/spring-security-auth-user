package com.example.training.util;

import org.springframework.stereotype.Component;

@Component
public class WebSvcUtil {
    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }
}
