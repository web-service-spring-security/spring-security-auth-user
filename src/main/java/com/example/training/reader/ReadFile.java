package com.example.training.reader;

import com.example.training.util.Constants;
import com.example.training.util.WebSvcUtil;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

@Data
@NoArgsConstructor
@Component
public class ReadFile {
    private static final Logger logger = LoggerFactory.getLogger(ReadFile.class);

    private static String fileCont;
    private static Path commonDir;
    private static Path dirProject=getDirPath();
    private static File dir = new File(dirProject.toString());
    private  static File[] directoryListing = dir.listFiles();
    static String keyFile = getTargetKeyFilePath(directoryListing, "_rsa");
    private static File file;

    static {
        assert keyFile != null;
        logger.info("=========== keyfile: " + keyFile);
        logger.info("=========== directoryListing: " + Arrays.toString(directoryListing));
        file = new File(keyFile);
    }

    String fileContent = buildContentKey(file, "Cp1250");

    public static Path getDirPath() {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext()) {
            context.refresh();
            Environment env = context.getEnvironment();
            String[] profiles = env.getActiveProfiles().length > 0 ? env.getActiveProfiles() : env.getDefaultProfiles();
            String profileType = env.getActiveProfiles().length > 0 ? Constants.TEST.getVarName() : Constants.DEFAULT.getVarName();

            buildDirProjectKey(Constants.SRC_TEST_RESOURCES.getVarName(), profiles, profileType, Constants.KEY.getVarName());

            if (WebSvcUtil.isNullOrEmpty(System.getenv(Constants.DOCKER_CONTAINER.getVarName()))
                    || !"true".equals(System.getenv(Constants.DOCKER_CONTAINER.getVarName()))) {
                if (!WebSvcUtil.isNullOrEmpty(System.getenv(Constants.SVC_ENV.getVarName()))) {
                    String javaAppRoot = System.getenv(Constants.JAVAAP_ROOT.getVarName());
                    String dirKeys = System.getenv(Constants.DIR_KEYS.getVarName());
                    buildDirProjectKey(javaAppRoot, profiles, Constants.DEFAULT.getVarName(), dirKeys);
                }

            } else {
                buildDirProjectKey(System.getenv(Constants.DOCKER_CONTAINER_APP_KEY.getVarName()), profiles, profileType, Constants.KEY.getVarName());
            }

        }
        return dirProject;
    }


    private static void buildDirProjectKey(String first, String[] profilesTab, String profileName, String key) {
        commonDir = Paths.get(first);
        if (Arrays.asList(profilesTab).contains(profileName)) {
            dirProject = commonDir.resolve(key);
        }
    }

    public String buildContentKey(File file, String charset) {
        if (!file.exists()) {
            logger.info("file doesn't exist");
            return fileCont;
        }

        StringBuilder fileContentBuilder = new StringBuilder();

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(file), charset))) {
            String line;
            while ((line = br.readLine()) != null) {
                fileContentBuilder.append(line.replace(" ", ""));
            }
        } catch (IOException e) {
            logger.error("file not found",e);
        }

        fileCont = fileContentBuilder.toString();
        return fileCont;
    }


    public static String getTargetKeyFilePath(File[] directoryListing, String keyFile) {
        if (directoryListing != null) {
            return Arrays.stream(directoryListing)
                    .filter(child -> child.getName().endsWith(keyFile))
                    .findFirst()
                    .map(File::getAbsolutePath)
                    .orElse(null);
        }
        return null;
    }
}
