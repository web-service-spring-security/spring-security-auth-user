# Stage 1: Clone the repository
FROM alpine/git:2.43.0 as clone
LABEL version=v1.0
LABEL app=marius-app
WORKDIR /app

RUN git clone https://gitlab.com/web-service-spring-security/spring-security-auth-user.git

# Stage 2: Build the project
FROM jelastic/maven:3.9.5-openjdk-21 as build
WORKDIR /app
COPY keys/id_rsa /app/spring-security-auth-user/src/main/resources/key/id_rsa
# Debug: Verify the key is copied
RUN ls -latr /app/spring-security-auth-user/src/main/resources/key

COPY --from=clone /app/spring-security-auth-user /app

# Ensure the keys directory is in the build context and correctly referenced
#COPY keys/id_rsa /app/src/main/resources/key/id_rsa
# Verify the app containt before packaging
RUN ls -latr /app

RUN mvn -DskipTests package

# Debug: Verify the JAR file is created
RUN ls -latr /app/target

# Stage 3: Create the runtime image
FROM openjdk:21-slim
WORKDIR /app

# Set environment variables for the final stage
ENV JAVA_APP=training-0.0.1-SNAPSHOT.jar
ENV SVC_ENV=dev
ENV DOCKER_CONTAINER=true
ENV DOCKER_CONTAINER_AP_KEY=/app/resources/

# Debug: Verify the app directory before copying the JAR file
RUN ls -latr /app

COPY --from=build /app/target/training-0.0.1-SNAPSHOT.jar training-0.0.1-SNAPSHOT.jar

#copy key file before starting the app
RUN mkdir -p /app/resources
COPY  keys/id_rsa /app/resources/key/id_rsa
# Debug: Verify the app directory after copying the JAR file
RUN ls -latr /app/resources/key

EXPOSE 8082
ENTRYPOINT ["java", "-jar", "training-0.0.1-SNAPSHOT.jar"]