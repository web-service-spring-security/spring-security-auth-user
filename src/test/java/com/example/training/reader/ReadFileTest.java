package com.example.training.reader;

import config.BaseTests;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.nio.file.Path;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class ReadFileTest extends BaseTests {
    @Autowired
    ReadFile readFile;
    private static final String KEY_DIR = "src\\test\\resources\\key";
    private Path dirPath = null;
    private File dir = null;
    private File[] directoryListing = null;
    private
    @BeforeEach
    void setUp() {
        dirPath = ReadFile.getDirPath();
        dir = new File(dirPath.toString());
        directoryListing = dir.listFiles();
    }

    @AfterEach
    void tearDown() {
        dirPath = null;
        dir = null;
        directoryListing = null;

    }

    @Test
    void getDirPath() {
        String strFilePath= dirPath.toString();
        Assertions.assertEquals(KEY_DIR,strFilePath.replace("/","\\"));
    }

    @Test
    void buldContentKey() {
        String keyFile = ReadFile.getTargetKeyFilePath(directoryListing, "_rsa");
        String srcKeyFile = keyFile.contains("src")?keyFile.substring(keyFile.indexOf("src")):"";
        File file = new File(srcKeyFile);
        String fileContent = readFile.buildContentKey(file, "Cp1250");
        boolean fileNotEmpty = !fileContent.isEmpty();
        Assertions.assertTrue(fileNotEmpty);
    }

    @Test
    void getTargetKeyFilePath() {
        String keyFile = ReadFile.getTargetKeyFilePath(directoryListing, "_rsa");
        String srcKeyFile = keyFile.contains("src")?keyFile.substring(keyFile.indexOf("src")):"";
        Assertions.assertEquals(KEY_DIR + "\\id_rsa",srcKeyFile.replace("/","\\"));
    }
}