package com.example.training.util;

public enum Constants {
    DOCKER_CONTAINER("DOCKER_CONTAINER"),
    DOCKER_CONTAINER_APP_KEY("DOCKER_CONTAINER_AP_KEY"),
    DEFAULT("default"),
    SVC_ENV("SVC_ENV"),
    KEY("key"),
    SRC_TEST_RESOURCES("src/test/resources/"),
    TEST("test"),
    JAVAAP_ROOT("JAVAAP_ROOT"),
    DIR_KEYS("DIR_KEYS");

    private final String varName;


    Constants(String varName) {
        this.varName=varName;

    }
    public String getVarName(){
        return varName;
    }
}
