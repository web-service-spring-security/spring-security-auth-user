package com.example.training.dto;

import com.example.training.model.Fourniture;

import java.util.List;

public record StudentDTO(int id,
						 String firstname,
						 String lastname,
						 Integer age,
						 String email,
						 List<Fourniture> fournitureList) { }