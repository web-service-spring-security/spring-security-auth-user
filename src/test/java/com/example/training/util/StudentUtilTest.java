package com.example.training.util;

import com.example.training.model.Fourniture;
import com.example.training.model.Student;
import com.example.training.repository.StudentRepository;
import com.example.training.service.StudentService;
import config.BaseTests;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
@SpringBootTest
@ExtendWith(MockitoExtension.class)
class StudentUtilTest extends BaseTests {
    @Autowired
    Environment environment;
    @Mock
    StudentRepository studentRepository;
    @InjectMocks
    StudentUtil studentUtil;
    @InjectMocks
    private StudentService studentService;
    Student student;
    Fourniture fourniture1;
    List<Fourniture> fournitureList = new ArrayList<>();
    boolean containsTest;
    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        studentUtil = new StudentUtil(studentRepository);

        containsTest = Arrays.asList(environment.getActiveProfiles()).contains("test");
        //given
        fourniture1 = new  Fourniture(1L,"",5,25, student);
        fournitureList.add(fourniture1);

        student  = new Student(1, "firstname",
                "lastNameD","tatalia@gmail.com","123456", LocalDate.now(),fournitureList);
        studentRepository.save(student);

    }

    @AfterEach
    void tearDown() {
        student = null;
        fournitureList = null;
        fourniture1 = null;
    }

    @Test
    void checkStudentExist() {
        if(containsTest) {
            Mockito.when(studentRepository.findById(student.getId())).thenReturn(Optional.of(student));
            Assertions.assertThat(studentUtil.checkStudentExist(student.getId())).isNotNull();
        }
    }
    @Test
    void checkStudentNotExist() {
        if(containsTest) {
            int studentId = 1;
            IllegalStateException exception = org.junit.jupiter.api.Assertions.assertThrows(
                    IllegalStateException.class, () -> {
                studentUtil.checkStudentExist(studentId);
            });
            org.junit.jupiter.api.Assertions.assertEquals("Student with id " + studentId + " does not exist my bobo", exception.getMessage());
        }
    }

}