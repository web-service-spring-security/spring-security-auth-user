package com.example.training.authfilter;

import config.BaseTests;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class JwtAuthenticationFilterTest extends BaseTests {
    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @InjectMocks
    private JwtAuthenticationFilter jwtAuthenticationFilter;
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
        response = null;
        request = null;
        filterChain = null;
        jwtAuthenticationFilter = null;
    }

    @Test
    void doFilterInternal() throws Exception {
        MockitoAnnotations.openMocks(this);
        // Appeler la méthode doFilterInternal() de la classe JwtAuthenticationFilter
        jwtAuthenticationFilter.doFilterInternal(request, response, filterChain);

        // Vérifier si la méthode doFilter() de FilterChain a été appelée une fois
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(request, response);
    }
}